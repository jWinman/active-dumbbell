#!/bin/bash

declare -a array
array=(0.03 0.05 0.07 0.10 0.20 0.30 0.40 0.50 0.70 0.80 0.90 1.00);

for i in ${array[@]};
do
	sed s/DUMMY/$i/ < MSD_raw.sh > MSD.sh$i;
	qsub MSD.sh$i;
done
