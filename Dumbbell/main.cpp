#include <iostream>
#include <fstream>
#include <omp.h>
#include <random>
#include <vector>

#include "dumbbell.h"
#include "../mpc.h"

// Parameter
double timeStepMPC;
double timeStepMD;
double T;
double MPCparticles;
double NumberDumb;
double alpha;
int Nx;
int Ny;
int Nz;
double forceX;
double forceY;
double forceZ;
double prop;
double l;
double k;
double eps;
double mass;
int writingNumber;
int equiTime;
int simTime;

std::vector<std::string> split(const std::string& s, const std::string& delim, const bool keep_empty = true)
{
    using namespace std;
    vector<string> result;
    if (delim.empty()) {
        result.push_back(s);
        return result;
    }
    string::const_iterator substart = s.begin(), subend;
    while (true) {
        subend = search(substart, s.end(), delim.begin(), delim.end());
        string temp(substart, subend);
        if (keep_empty || !temp.empty()) {
            result.push_back(temp);
        }
        if (subend == s.end()) {
            break;
        }
        substart = subend + delim.size();
    }
    return result;
}

void readParameters(double prop_tmp)
{
    using namespace std;
    vector<vector<string>> lines;
    string line;
    ifstream parameters("parameter.ini");
    if (parameters.is_open())
    {
	while (getline(parameters, line))
	{
	    lines.push_back(split(line, " "));

	}
	parameters.close();
    }
    else
    {
	cout << "Unable to open file\n";
    }

    // Parameter
    timeStepMPC = stod(lines[0][1]);
    timeStepMD = stod(lines[1][1]);
    T = stod(lines[2][1]);
    MPCparticles = stod(lines[3][1]);
    NumberDumb = stod(lines[4][1]);
    alpha = 2 * M_PI * stod(lines[5][1]) / 360.;
    Nx = stod(lines[6][1]);
    Ny = stod(lines[7][1]);
    Nz = stod(lines[8][1]);
    forceX = stod(lines[9][1]);
    forceY = stod(lines[10][1]);
    forceZ = stod(lines[11][1]);
    prop = prop_tmp;
    l  = stod(lines[13][1]);
    k  = stod(lines[14][1]);
    eps = stod(lines[15][1]);
    mass = stod(lines[16][1]);
    writingNumber = stod(lines[17][1]);
    equiTime = stod(lines[18][1]);
    simTime = stod(lines[19][1]);
}

void printEnergy(std::vector<double> kinE, std::vector<double> potE)
{
    printf("Output: Energy \n");
    char charE[100];
    sprintf(charE, "Daten/energy.csv");
    FILE *fileE = fopen(charE, "w");
    for (unsigned int t = 0; t < kinE.size(); t++)
    {
	fprintf(fileE, "%d %.15f %.15f %.15f\n", t, kinE[t], potE[t], kinE[t] + potE[t]);
    }
    fclose(fileE);
}

//int main()
int main(int argc, char* argv[])
{
    readParameters(std::stod(argv[1]));

    unsigned int seed = std::random_device()();
    printf("seed: %u \n", seed);
    std::vector<std::mt19937> generators(omp_get_max_threads());
    for (size_t i = 0; i < generators.size(); i++)
    {
    	generators[i] = std::mt19937(seed + i);
    }

    Dumbbell system(NumberDumb, timeStepMD, Nx, Ny, Nz, l, k, eps, mass);
    Mpc mpc(timeStepMPC, Nx, Ny, Nz, MPCparticles, NumberDumb, T, alpha, false, false, false, 1, mass, generators);

    int MDtime = timeStepMPC / timeStepMD;
    std::vector<double> kinE((simTime - equiTime) / writingNumber);
    std::vector<double> potE((simTime - equiTime) / writingNumber);

    Eigen::Vector3d force;
    force[0] = forceX;
    force[1] = forceY;
    force[2] = forceZ;
    std::vector<Eigen::Vector3d> r(NumberDumb);
    std::vector<Eigen::Vector3d> v(NumberDumb);

    system.calcForces();
    for (int t = 0; t < simTime; t++)
    {
	std::tie(r, v) = system.GetPosVel();
	mpc.setMDparticles(r, v);
    	mpc.reset_Parameter();
    	mpc.v_center_of_mass();
    	mpc.MCthermostat(0.1);
    	mpc.collision();
    	mpc.propulsion(prop);
    	mpc.streaming(force);

    	system.SetVel(mpc.getMDparticles());
    	for (int tMD = 0; tMD < MDtime; tMD++)
    	{
	    system.Verlet();
	}

	if (t >= equiTime)
	{
	    int ttmp = t - equiTime;
    	    system.Output(ttmp, prop);
	    if (ttmp % writingNumber == 0)
	    {
	    	kinE[ttmp / writingNumber] = mpc.kinE();
	    	potE[ttmp / writingNumber] = system.EPot();
	    }
	}
    }

    printEnergy(kinE, potE);

    return 0;
}
