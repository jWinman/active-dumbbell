import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mayavi import mlab

_, parameter = np.genfromtxt("parameter.ini", unpack=True)
Nx, Ny, Nz = parameter[6:9]
abmessungen = [0, Nx, 0, Ny, 0, Nz]
NumberDumb = parameter[4]
simTime = int(parameter[-1])

boundaries = lambda x, l: x  - l * np.floor(x / l)

x, y, z = np.loadtxt("Daten/positions.csv", unpack=True)
figMayavi = mlab.figure(figure=1, bgcolor=(0.5, 0.2, 0.5), size=(1000, 1000))
pts = mlab.points3d(x[:2], y[:2], z[:2], resolution=40, scale_factor=0.5)
ms_pts = pts.mlab_source

mlab.axes(extent=abmessungen)

movie = []
for i in range(len(x)):
    movie.append(mlab.screenshot(antialiased=True))
    mlab.view(azimuth=0, elevation=80, distance=35)
    xi, yi, zi = x[i:i+2], y[i:i+2], z[i:i+2]
    xi, yi, zi = boundaries(xi, Nx), boundaries(yi, Ny), boundaries(zi, Nz)
    ms_pts.set(x=xi, y=yi, z=zi)

fig = plt.figure(figsize=(4, 3), tight_layout=True, frameon=False)
ax = fig.add_subplot(1, 1, 1)
ax.axes.get_xaxis().set_visible(False)
ax.axes.get_yaxis().set_visible(False)
imshow = None

def init():
    global imshow
    imshow = ax.imshow([[]])
    return []

def animation(i):
    global imshow
    imshow.remove()
    imshow = ax.imshow(movie[i])
    return []

anim = mpl.animation.FuncAnimation(fig, animation, init_func=init, frames=simTime)
anim.save("Plots/animation.mp4", fps=60, dpi=320)
